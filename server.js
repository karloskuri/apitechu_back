var express = require('express');
var app = express();

//config
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
const URL_BASE = '/techu/';

app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(bodyParser.json());

//routes
app.use(URL_BASE  + 'users',require('./routes/users'));
app.use(URL_BASE  + 'auth',require('./routes/auth'));

app.listen(port, function() {
    console.log('Example app listening on port 30000!');
});
