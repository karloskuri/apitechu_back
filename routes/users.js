const { Router } = require('express');
const router = Router();
const _ = require('underscore');
const user_file = require('../data/users.json');

//routes
//Petición GET users
router.get('/', function (req, res) {
  res.status(201).send(user_file);
});

//Peticiòn GET users con ID
router.get('/:id', function (req, res) {
   let pos = req.params.id - 1;
   let respuesta = (user_file[pos] == undefined) ? {"msg":"Usuario mo encontrado"} : user_file[pos];
   res.send(respuesta);
 });

// POST users
router.post('/', function(req, res) {
  let newUser = {
      "id": user_file.length + 1,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password
    };
   user_file.push(newUser);
   res.send(newUser);
});


// PUT users
router.put('/:id', function(req, res) {
  let pos = req.params.id - 1;
  let updatedUser = {
     "id": req.params.id,
     "first_name": req.body.first_name,
     "last_name": req.body.last_name,
     "email": req.body.email,
     "password": req.body.password
  };
  user_file[pos] = updatedUser;
  res.send(updatedUser);
});

//DEL users
router.delete('/:id', function (req, res) {
  const {id} = req.params;
  let pos = user_file.length;
  _.each(user_file, (user, i) => {
    if (user.ID == id) {
      pos = i;
//      user_file.splice(i+1, 1);
    }
  });

  if (user_file[pos] == undefined) {
    res.send({"msg":"Usuario mo encontrado"});
  }
  else {
    user_file.splice(pos, 1);
    res.send(user_file);
  }
});

module.exports = router;
