const { Router } = require('express');
const router = Router();
const _ = require('underscore');
const user_file = require('../data/users.json');
var bodyParser = require('body-parser');

//routes
// POST login
router.post('/login', function(req, res) {
  let i = 0;
  let encontrado = false;
  while ((i < user_file.length) && !encontrado) {
    if (user_file[i].email == req.body.email && user_file[i].password == req.body.password) {
        encontrado = true;
        user_file[i].logged = true;
      //  writeUserDataToFile(user_file);
    }
    i++;
  }
  if (encontrado) {
    res.send({"msg":"Usuario logueado correctamente", "id":i, "email": user_file[i].email});
  }
  else {
     res.send({"msg":"Usuario no encontrado"});
  }
});

// PUT logout
router.put('/logout/:id', function(req, res) {
  let i = 0;
  let encontrado = false;
  while ((i < user_file.length) && !encontrado) {
    if (user_file[i].ID == req.params.id && user_file[i].logged==true) {
        encontrado = true;
        delete user_file[i].logged;
      //  writeUserDataToFile(user_file);
    }
    i++;
  }
  if (encontrado) {
    res.send({"msg":"Usuario deslogueado correctamente", "id":i, "email": user_file[i].email});
  }
  else {
     res.send({"msg":"Usuario no encontrado"});
  }
});

// Funcion de persistencia
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("../data/users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };


module.exports = router;
